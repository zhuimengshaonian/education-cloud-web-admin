import axios from 'axios'
import qs from 'qs'
import http from '@/utils/httpRequest'
import store from '@/store'

export function loginByUserName (userInfo) {
  let storeUserInfo = store.getters['user/getUserInfo']
  userInfo.client_id = storeUserInfo.client_id
  userInfo.grant_type = storeUserInfo.grant_type
  userInfo.client_secret = storeUserInfo.client_secret
  userInfo.scope = storeUserInfo.scope
  let data = {
    key: userInfo.key,
    code: userInfo.code,
    username: userInfo.username,
    password: userInfo.password,
    grant_type: userInfo.grant_type,
    scope: userInfo.scope,
    client_id: userInfo.client_id,
    client_secret: userInfo.client_secret
  }
  return axios.post(http.httpUrl('/auth/oauth/token'), qs.stringify(data))
}
